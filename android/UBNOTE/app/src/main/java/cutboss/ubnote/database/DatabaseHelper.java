/*
 * Copyright (C) 2018 CUTBOSS
 */

package cutboss.ubnote.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DatabaseHelper.
 *
 * @author CUTBOSS
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    /** TAG */
    private static final String TAG = "DatabaseHelper";

    /** If you change the database schema, you must increment the database version */
    private static final int DATABASE_VERSION = 1;

    /** DB Name */
    private static final String DATABASE_NAME = "UBNote.db";

    /** Data Type */
    static final String INTEGER_TYPE = " INTEGER";
    static final String REAL_TYPE = " REAL";
    static final String TEXT_TYPE = " TEXT";
    static final String BLOB_TYPE = " BLOB";
    static final String COMMA_SEP = ",";

    /** Singleton Instance */
    private static DatabaseHelper sSingleton = null;

    /**
     * Get the singleton instance.
     *
     * @param context Context
     * @return DatabaseHelper
     */
    static synchronized DatabaseHelper getInstance(Context context) {
        if (null == sSingleton) {
            sSingleton = new DatabaseHelper(context);
        }
        return sSingleton;
    }

    /**
     * Constructor.
     *
     * @param context Context
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table
        db.execSQL(NoteData.SQL_CREATE_TABLE_NOTES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
