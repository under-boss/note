/*
 * Copyright (C) 2018 CUTBOSS
 */

package cutboss.ubnote;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cutboss.ubnote.database.NoteData;

import static cutboss.ubnote.content.Intent.EXTRA_ID;

/**
 * ListActivity.
 *
 * @author CUTBOSS
 */
public class ListActivity extends AppCompatActivity {
    /** TAG */
    private static final String TAG = "ListActivity";

    /** Note Data */
    private NoteData mNoteData;

    /** Recycler View Adapter */
    private RecyclerView.Adapter mRecyclerViewAdapter;

    /** Notes */
    private List<Map<String, Object>> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // lists notes.
        list();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // feedback
        if (id == R.id.action_feedback) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=cutboss.ubnote");
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialize.
     */
    private void initialize() {
        // set layout
        setContentView(R.layout.activity_list);

        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set fab
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), EditActivity.class));
            }
        });

        // set recycler view
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(getBaseContext(), linearLayoutManager.getOrientation());
        // api level less than 24?
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Drawable divider = ContextCompat.getDrawable(getBaseContext(), R.drawable.divider);
            if (null != divider) {
                dividerItemDecoration.setDrawable(divider);
            }
        }
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mRecyclerViewAdapter = new RecyclerView.Adapter() {
            @Override
            public int getItemCount() {
                return mList.size();
            }

            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(
                    @NonNull ViewGroup parent, int viewType) {
                return new ViewHolder(
                        ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                                .inflate(R.layout.list_item, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                // get item view
                View itemView = holder.itemView;

                // get item
                Map<String, Object> item = mList.get(position);

                // set sentence
                TextView sentenceView = itemView.findViewById(R.id.sentence);
                sentenceView.setText((String) item.get(NoteData.KEY_SENTENCE));

                // set updated at
                TextView updatedAtView = itemView.findViewById(R.id.updated_at);
                updatedAtView.setText(formatDate(item.get(NoteData.KEY_UPDATED_AT)));

                // get _id
                final long _id = (long) item.get(NoteData.KEY_ID);

                // click
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), EditActivity.class);
                        intent.putExtra(EXTRA_ID, _id);
                        startActivity(intent);
                    }
                });
            }

            /**
             * Format date.
             *
             * @param obj Object
             * @return Formatted date
             */
            private String formatDate(Object obj) {
                // get default locale
                Locale locale = Locale.getDefault();

                // set pattern
                String pattern = "yyyy/MM/dd HH:mm";

                // formatted string
                return new SimpleDateFormat(pattern, locale).format(obj);
            }

            /**
             * ViewHolder.
             */
            class ViewHolder extends RecyclerView.ViewHolder {
                ViewHolder(View itemView) {
                    super(itemView);
                }
            }
        });

        // get note data
        mNoteData = new NoteData(getBaseContext());
    }

    /**
     * Lists the notes.
     */
    private void list() {
        mList.clear();
        mList.addAll(mNoteData.list());
        mRecyclerViewAdapter.notifyDataSetChanged();
    }
}
