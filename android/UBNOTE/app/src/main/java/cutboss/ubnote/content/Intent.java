/*
 * Copyright (C) 2018 CUTBOSS
 */

package cutboss.ubnote.content;

/**
 * Intent.
 *
 * @author CUTBOSS
 */
public class Intent {
    /** EXTRA: ID */
    public static final String EXTRA_ID = "cutboss.ubnote.intent.extra.ID";
}
