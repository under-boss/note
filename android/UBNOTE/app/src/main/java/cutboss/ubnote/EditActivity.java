/*
 * Copyright (C) 2018 CUTBOSS
 */

package cutboss.ubnote;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import cutboss.ubnote.database.NoteData;

import static cutboss.ubnote.content.Intent.EXTRA_ID;

/**
 * EditActivity.
 *
 * @author CUTBOSS
 */
public class EditActivity extends AppCompatActivity {
    /** TAG */
    private static final String TAG = "EditActivity";

    /** Edit Text */
    private EditText mEditText;

    /** Note Data */
    private NoteData mNoteData;

    /** ID */
    private long mId = 0L;

    /** File Name */
    private String mFileName = "";

    /** Text */
    private String mText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize
        initialize();

        // Intent
        Intent intent = getIntent();

        // get _id
        if (intent.hasExtra(EXTRA_ID)) {
            mId = intent.getLongExtra(EXTRA_ID, 0L);

            // get note
            Map<String, Object> note = mNoteData.get(mId);

            // get file name
            mFileName = (String) note.get(NoteData.KEY_FILE_NAME);

            // get text
            mText = readFile(mFileName);

            // set text
            mEditText.setText(mText);

            // set focus to the back
            mEditText.setSelection(mText.length());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        if (0L == mId) {
            menu.findItem(R.id.action_delete).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle action bar item clicks here. the action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // save
        if (id == R.id.action_save) {
            if (0L < mId) {
                update();
            } else {
                // create note
                create();
            }
            return true;
        }

        // delete
        if (id == R.id.action_delete) {
            delete();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Initialize.
     */
    private void initialize() {
        // set layout
        setContentView(R.layout.activity_edit);

        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            // show back
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // get edit text
        mEditText = findViewById(R.id.edit_text);

        // get note data
        mNoteData = new NoteData(getBaseContext());
    }

    /**
     * Creates the new note.
     */
    private void create() {
        // get text
        String text = getText();

        // no change?
        if ("".equals(text)) {
            // finish
            finish();
            return;
        }

        // create file name
        String fileName = System.currentTimeMillis() + ".txt";

        // write file
        if (writeFile(fileName, text)) {
            // success

            // create note data
            mNoteData.create(fileName, getSentence(text, 100));
        }

        // finish
        finish();
    }

    /**
     * Updates the note.
     */
    private void update() {
        // get text
        String text = getText();

        // no change
        if (mText.equals(text)) {
            finish();
            return;
        }

        // write file
        if (writeFile(mFileName, text)) {
            // success

            // update note data
            mNoteData.update(mId, mFileName, getSentence(text, 100));
        }

        // finish
        finish();
    }

    /**
     * Deletes the note.
     */
    private void delete() {
        // delete file
        if (deleteFile(mFileName)) {
            // success

            // delete note data
            mNoteData.delete(mId);
        }

        // finish
        finish();
    }

    /**
     * Get the text.
     *
     * @return Text
     */
    private String getText() {
        return mEditText.getText().toString();
    }

    /**
     * Write to the file.
     *
     * @param fileName File name
     * @param text Text
     * @return Result
     */
    private boolean writeFile(String fileName, String text) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(fileName, MODE_PRIVATE);
            fos.write(text.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Read from the file.
     *
     * @param fileName File name
     * @return Text
     */
    private String readFile(String fileName) {
        try {
            FileInputStream fis = openFileInput(fileName);
            byte[] bytes = new byte[fis.available()];
            if (0 > fis.read(bytes)) {
                return "";
            }
            return new String(bytes);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Get the sentence of text.
     *
     * @param text Text
     * @param maxLength Max length
     * @return Sentence
     */
    private String getSentence(String text, int maxLength) {
        // invalid?
        if ((null == text) || "".equals(text)) {
            return "";
        }
        if (1 > maxLength) {
            return "";
        }

        // get paragraph
        String[] paragraph = text.split("[\r\n]");
        for (String sentence : paragraph) {
            // empty?
            if ("".equals(sentence)) {
                // skip
                continue;
            }
            if (maxLength < sentence.length()) {
                return sentence.substring(0, maxLength);
            }
            return sentence;
        }
        return "";
    }
}
