/*
 * Copyright (C) 2018 CUTBOSS
 */

package cutboss.ubnote.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * NoteData.
 *
 * @author CUTBOSS
 */
public class NoteData {
    /** TAG */
    private static final String TAG = "NoteData";

    /** KEY */
    public static final String KEY_ID = "_id";
    public static final String KEY_FILE_NAME = "file_name";
    public static final String KEY_SENTENCE = "sentence";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_UPDATED_AT = "updated_at";
    public static final String KEY_SAVED_AT = "saved_at";

    /** Inner class that defines the table contents */
    static abstract class Notes implements BaseColumns {
        static final String TABLE_NAME = "notes";
        static final String COLUMN_NAME_FILE_NAME = "file_name";
        static final String COLUMN_NAME_SENTENCE = "sentence";
        static final String COLUMN_NAME_CREATED_AT = "created_at";
        static final String COLUMN_NAME_UPDATED_AT = "updated_at";
        static final String COLUMN_NAME_SAVED_AT = "saved_at";
    }

    /** SQL */
    static final String SQL_CREATE_TABLE_NOTES =
            "CREATE TABLE " + Notes.TABLE_NAME + " (" +
                    Notes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    Notes.COLUMN_NAME_FILE_NAME + DatabaseHelper.TEXT_TYPE + DatabaseHelper.COMMA_SEP +
                    Notes.COLUMN_NAME_SENTENCE + DatabaseHelper.TEXT_TYPE + DatabaseHelper.COMMA_SEP +
                    Notes.COLUMN_NAME_CREATED_AT + DatabaseHelper.INTEGER_TYPE + DatabaseHelper.COMMA_SEP +
                    Notes.COLUMN_NAME_UPDATED_AT + DatabaseHelper.INTEGER_TYPE + DatabaseHelper.COMMA_SEP +
                    Notes.COLUMN_NAME_SAVED_AT + DatabaseHelper.INTEGER_TYPE +
                    " )";

    /** Database Helper */
    private DatabaseHelper mDbHelper;

    /**
     * Constructor.
     *
     * @param context Context
     */
    public NoteData(Context context) {
        mDbHelper = DatabaseHelper.getInstance(context);
    }

    // ---------------------------------------------------------------------------------------------
    // CREATE
    // ---------------------------------------------------------------------------------------------

    /**
     * Creates the new note.
     *
     * @param fileName File name
     * @param sentence Sentence
     * @return ID
     */
    public long create(String fileName, String sentence) {
        return create(fileName, sentence, 0L, 0L, 0L);
    }

    /**
     * Creates the new note.
     *
     * @param fileName File name
     * @param sentence Sentence
     * @param createdAt Created at
     * @param updatedAt Updated at
     * @param savedAt Saved at
     * @return ID
     */
    public long create(
            String fileName, String sentence, long createdAt, long updatedAt, long savedAt) {
        // set created at
        if (1L > createdAt) {
            // now
            createdAt = System.currentTimeMillis();
        }

        // set updated at
        if (1L > updatedAt) {
            updatedAt = createdAt;
        }

        // set saved at
        if (1L > savedAt) {
            savedAt = updatedAt;
        }

        // set _id
        long _id = -1L;

        // gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // begin transaction
        db.beginTransaction();
        try {
            // insert
            final SQLiteStatement stmtInsert = db.compileStatement(
                    "INSERT INTO " + Notes.TABLE_NAME + " (" +
                            Notes.COLUMN_NAME_FILE_NAME + DatabaseHelper.COMMA_SEP +
                            Notes.COLUMN_NAME_SENTENCE + DatabaseHelper.COMMA_SEP +
                            Notes.COLUMN_NAME_CREATED_AT + DatabaseHelper.COMMA_SEP +
                            Notes.COLUMN_NAME_UPDATED_AT + DatabaseHelper.COMMA_SEP +
                            Notes.COLUMN_NAME_SAVED_AT +
                            ") values (?, ?, ?, ?, ?);");
            stmtInsert.bindString(1, fileName);
            stmtInsert.bindString(2, sentence);
            stmtInsert.bindLong(3, createdAt);
            stmtInsert.bindLong(4, updatedAt);
            stmtInsert.bindLong(5, savedAt);
            _id = stmtInsert.executeInsert();
            stmtInsert.close();

            // set transaction successful
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // end transaction
            db.endTransaction();
        }

        return _id;
    }

    // ---------------------------------------------------------------------------------------------
    // DELETE
    // ---------------------------------------------------------------------------------------------

    /**
     * Deletes the note by ID.
     *
     * @param _id ID
     * @return result
     */
    public boolean delete(long _id) {
        // delete
        boolean delete = false;

        // gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // begin transaction
        db.beginTransaction();
        try {
            // select
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + Notes.TABLE_NAME + " WHERE " + Notes._ID + " = ?",
                    new String[]{Long.toString(_id)});
            if (0 < cursor.getCount()) {
                // delete
                SQLiteStatement stmtDelete = db.compileStatement(
                        "DELETE FROM " + Notes.TABLE_NAME
                                + " WHERE " + Notes._ID
                                + " IN (?)");
                stmtDelete.bindLong(1, _id);
                stmtDelete.executeUpdateDelete();
                stmtDelete.close();

                // delete
                delete = true;
            }
            cursor.close();

            // set transaction successful
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // end transaction
            db.endTransaction();
        }

        return delete;
    }

    // ---------------------------------------------------------------------------------------------
    // GET
    // ---------------------------------------------------------------------------------------------

    /** Projection */
    private static final String[] PROJECTION = {
            Notes._ID,
            Notes.COLUMN_NAME_FILE_NAME,
            Notes.COLUMN_NAME_SENTENCE,
            Notes.COLUMN_NAME_CREATED_AT,
            Notes.COLUMN_NAME_UPDATED_AT,
            Notes.COLUMN_NAME_SAVED_AT
    };

    /**
     * Gets the note by ID.
     *
     * @param _id ID
     * @return note
     */
    public Map<String, Object> get(long _id) {
        // gets the data repository in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = PROJECTION;

        // set selection
        String selection = Notes._ID + " = ?";
        String[] selectionArgs = {Long.toString(_id)};

        // how you want the results sorted in the resulting cursor
        String sortOrder = Notes.COLUMN_NAME_UPDATED_AT + " DESC, " + Notes._ID + " DESC";

        // query
        Cursor cursor = db.query(
                Notes.TABLE_NAME,    // The table to query
                projection,           // The columns to return
                selection,            // The columns for the WHERE clause
                selectionArgs,        // The values for the WHERE clause
                null,        // don't group the rows
                null,         // don't filter by row groups
                sortOrder             // The sort order
        );

        // get note
        Map<String, Object> note;
        if ((0 < cursor.getCount()) && cursor.moveToFirst()) {
            note = get(cursor);
        } else {
            note = new HashMap<>();
        }
        cursor.close();
        return note;
    }

    /**
     * Gets the note by cursor.
     *
     * @param cursor Cursor
     * @return note
     */
    private Map<String, Object> get(Cursor cursor) {
        // get _id
        final long _id = cursor.getLong(cursor.getColumnIndexOrThrow(Notes._ID));

        // get file name
        final String fileName = cursor.getString(
                cursor.getColumnIndexOrThrow(Notes.COLUMN_NAME_FILE_NAME));

        // get sentence
        final String sentence = cursor.getString(
                cursor.getColumnIndexOrThrow(Notes.COLUMN_NAME_SENTENCE));

        // get created at
        final long createdAt = cursor.getLong(
                cursor.getColumnIndexOrThrow(Notes.COLUMN_NAME_CREATED_AT));

        // get updated at
        final long updatedAt = cursor.getLong(
                cursor.getColumnIndexOrThrow(Notes.COLUMN_NAME_UPDATED_AT));

        // get saved at
        final long savedAt = cursor.getLong(
                cursor.getColumnIndexOrThrow(Notes.COLUMN_NAME_SAVED_AT));

        // set note
        Map<String, Object> note = new HashMap<>();
        note.put(KEY_ID, _id);
        note.put(KEY_FILE_NAME, fileName);
        note.put(KEY_SENTENCE, sentence);
        note.put(KEY_CREATED_AT, createdAt);
        note.put(KEY_UPDATED_AT, updatedAt);
        note.put(KEY_SAVED_AT, savedAt);
        return note;
    }

    // ---------------------------------------------------------------------------------------------
    // LIST
    // ---------------------------------------------------------------------------------------------

    /**
     * Lists the notes.
     *
     * @return notes
     */
    public List<Map<String, Object>> list() {
        // gets the data repository in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = PROJECTION;

        // how you want the results sorted in the resulting cursor
        String sortOrder = Notes.COLUMN_NAME_UPDATED_AT + " DESC, " + Notes._ID + " DESC";

        // query
        Cursor cursor = db.query(
                Notes.TABLE_NAME,    // The table to query
                projection,           // The columns to return
                null,        // The columns for the WHERE clause
                null,    // The values for the WHERE clause
                null,        // don't group the rows
                null,         // don't filter by row groups
                sortOrder             // The sort order
        );

        // get notes
        List<Map<String, Object>> notes = new ArrayList<>();
        if ((0 < cursor.getCount()) && cursor.moveToFirst()) {
            do {
                // add note
                notes.add(get(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return notes;
    }

    // ---------------------------------------------------------------------------------------------
    // UPDATE
    // ---------------------------------------------------------------------------------------------

    /**
     * Updates the note.
     *
     * @param _id ID
     * @param fileName File name
     * @param sentence Sentence
     * @return update
     */
    public boolean update(long _id, String fileName, String sentence) {
        return update(_id, fileName, sentence, System.currentTimeMillis());
    }

    /**
     * Updates the note.
     *
     * @param _id ID
     * @param fileName File name
     * @param sentence Sentence
     * @param updatedAt Updated at
     * @return result
     */
    public boolean update(long _id, String fileName, String sentence, long updatedAt) {
        // set updated at
        if (1L > updatedAt) {
            // now
            updatedAt = System.currentTimeMillis();
        }

        // set saved at
        long savedAt = updatedAt;

        // update
        boolean update = false;

        // gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // begin transaction
        db.beginTransaction();
        try {
            // select
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + Notes.TABLE_NAME + " WHERE " + Notes._ID + " = ?",
                    new String[]{Long.toString(_id)});
            if (0 < cursor.getCount()) {
                // update
                SQLiteStatement stmtUpdate = db.compileStatement(
                        "UPDATE " + Notes.TABLE_NAME
                                + " SET " + Notes.COLUMN_NAME_FILE_NAME + " = ?, "
                                + Notes.COLUMN_NAME_SENTENCE + " = ?, "
                                + Notes.COLUMN_NAME_UPDATED_AT + " = ?, "
                                + Notes.COLUMN_NAME_SAVED_AT + " = ?"
                                + " WHERE " + Notes._ID + " IN (?)");
                stmtUpdate.bindString(1, fileName);
                stmtUpdate.bindString(2, sentence);
                stmtUpdate.bindLong(3, updatedAt);
                stmtUpdate.bindLong(4, savedAt);
                stmtUpdate.bindLong(5, _id);
                stmtUpdate.executeUpdateDelete();
                stmtUpdate.close();

                // update
                update = true;
            }
            cursor.close();

            // set transaction successful
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // end transaction
            db.endTransaction();
        }

        return update;
    }
}
